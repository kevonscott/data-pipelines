"""A simple COVID19 data pipeline with daily reports from 
    John Hopkins University’s GitHub.
"""

import re
import sqlite3
import requests
import pandas as pd

def get_data_uris():
    # https://github.com/CSSEGISandData/COVID-19/tree/master/csse_covid_19_data/csse_covid_19_daily_reports
    url = (
        'https://api.github.com/repos/CSSEGISandData/COVID-19/contents/'
        'csse_covid_19_data/csse_covid_19_daily_reports'
    )
    data_uris = []

    # Match paterns of dd-dd-dddd. eg 01-20-2020
    pattern = re.compile('\d{2}-\d{2}-\d{4}.csv')
    res = requests.get(url)
    res = res.json()

    data_uris = [i['download_url'] for i in res if pattern.match(i['name'])]
    return data_uris

def load_transform_data(uri):
    column_mapper = {
        'Last Update': 'Last_Update',
        'Lat': 'Latitude',
        'Long_': 'Longitude',
        'Province/State': 'Province_State',
        'Country/Region': 'Country_Region'
    }
    target_features = ['Province_State', 'Country_Region', 'Last_Update', 'Confirmed', 'Deaths', 'Recovered']
    data = pd.read_csv(uri)
    data = data.rename(columns=column_mapper)
    return(data[target_features])

def upload_to_db(df: pd.DataFrame, replace=False):
    db_name = 'covid.db'
    conn = sqlite3.connect(db_name)   # Create DB Connection
    if replace:
        df.to_sql(name=db_name, con=conn,index = False, if_exists='replace')
    else:
        df.to_sql(name=db_name, con=conn,index = False, if_exists='append')
    conn.commit()
    conn.close()

def main():
    uris = get_data_uris()
    for uri in uris:
        df = load_transform_data(uri)
        if uris.index(uri) == 0:
            upload_to_db(df=df, replace=True)
        else:
            upload_to_db(df=df, replace=False)


if __name__=='__main__':
    main()