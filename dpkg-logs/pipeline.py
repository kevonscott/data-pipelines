import sys
import time
import sys
import sqlite3
import pkg_resources
from datetime import datetime


from log_generator import LOG_A, LOG_B, MAX_LOG_LENGTH

DB_NAME = pkg_resources.resource_filename(__name__, 'db.sqlite')


def parse_entry(entry):
    entry = entry.split(" ")
    if len(entry)<6:
        return None

    log_time = f'{entry[0]} {entry[1]}'
    operation = entry[2]
    unknown1 = entry[3]
    unknown2 = entry[4]
    unknown3 = entry[5]
    created = datetime.now().strftime("%Y-%m-%dT%H:%M:%S")

    return [log_time, operation, unknown1, unknown2, unknown3, created]

class Database:
    def __init__(self, db_name) -> None:
        self.db_name = db_name

    def create_db(self, table_name='dpkg_logs'):
        query = f"""
        CREATE TABLE IF NOT EXISTS {table_name} (
            time TEST NOT NULL UNIQUE,
            operation TEXT,
            unknown1 TEXT,
            unknown2 TEXT,
            unknown3 TEXT,
            created DATETIME DEFAULT CURRENT_TIMESTAMP
        )
        """
        conn = sqlite3.connect(database=self.db_name)
        conn.execute(query)
        conn.commit()
        conn.close()

    def insert(self, entry, table_name='dpkg_logs'):
        conn = sqlite3.connect(database=self.db_name)
        cur = conn.cursor()
        cur.execute(f'INSERT INTO {table_name} VALUES (?,?,?,?,?,?)', entry)
        conn.commit()
        conn.close()


def main():
    db = Database(db_name=DB_NAME)
    db.create_db()

    try:
        log_file_a = open(LOG_A, 'r')
        log_file_b = open(LOG_B, 'r')

        while True:
            where_a = log_file_a.tell()
            where_b = log_file_b.tell()

            # if where_a >= MAX_LOG_LENGTH:
            #     log_file_a.seek(0)
            # if where_b >= MAX_LOG_LENGTH:
            #     log_file_b.seek(0)

            line_a = log_file_a.readline()
            line_b = log_file_b.readline()

            if not line_a and not line_b:
                time.sleep(1)
                log_file_a.seek(where_a)
                log_file_b.seek(where_b)
                continue
            else:
                if line_a:
                    entry = line_a
                    which_file = 'FileA'
                else:
                    entry = line_b
                    which_file = 'FileB'
                entry = parse_entry(entry.rstrip())
                print(f'Found NEW ENTRY ({which_file}): {entry}')
                if entry and len(entry)<=6:
                    print(f'inserting in db: {entry}')
                    db.insert(entry=entry,)
    except Exception as e:
        print('Some exception:')
        print(e)
    finally:
        log_file_a.close()
        log_file_b.close()
        sys.exit()
if __name__=='__main__':
    main()