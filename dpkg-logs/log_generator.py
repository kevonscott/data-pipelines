import sys
import random
import time
import pkg_resources
from datetime import datetime

# Script for continuously generating fake logs and rotating log files

# /var/log/dpkg.log
# general format
LOG_FORMAT = "{date_time} {type} {unknown1} {unknown2} {unknown3}"

LOG_A = pkg_resources.resource_filename(__name__, "log_a.txt")
LOG_B = pkg_resources.resource_filename(__name__, "log_b.txt")
MAX_LOG_LENGTH = 10

def generate_log_entry():
    now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    operation_type = random.choice(
        ['status', 'remove', 'purge', 'trigpoc', 'install', 'configure']
    )
    unknown1 = random.choice(['installed', 'half-configured', 'unpacked'])
    unknown2 = random.choice(['libxxf86vm1:amd64', 'libflac8:amd64', 'libxcb-present0:amd64'])
    unknown3 = f'{random.randint(1, 10)}.{random.randint(1, 10)}.{random.randint(1, 10)}-build'
    entry = LOG_FORMAT.format(
        date_time=now,
        type=operation_type,
        unknown1=unknown1,
        unknown2=unknown2,
        unknown3=unknown3,
    )
    return entry

def clear_log_file(log_file):
    with open(log_file, 'w+') as f:
        f.write("")

def write_log(log_file, entry):
    with open(log_file, 'a') as f:
        f.write(f'{entry}\n')

def main():
    log_file_tracker = LOG_A
    log_entry_counter = 0

    clear_log_file(LOG_A)
    clear_log_file(LOG_B)

    while True:
        try:
            entry = generate_log_entry()
            print(f'new log entry({log_file_tracker}): {entry}')
            write_log(log_file_tracker, entry)
            log_entry_counter += 1
            if log_entry_counter >= MAX_LOG_LENGTH:
                if log_file_tracker == LOG_A:
                    log_file_tracker = LOG_B
                else:
                    log_file_tracker = LOG_A
                clear_log_file(log_file_tracker)
                log_entry_counter = 0
            time.sleep(random.choice([1,2,3,4,5]))
        except KeyboardInterrupt:
            sys.exit()

if __name__=='__main__':
    main()